/*a. Find all artists that has letter d in its name.*/
select name as 'artist_name' from artists
where name like '%d%';

/*b. Find all songs that has a length of less than 3:50*/
select song_name from songs
where length < 350;

/*c. Join the 'albums' and 'songs' tables. (Only show the album name, song name and song length)*/
select al.album_title, so.song_name, so.length as 'song length' 
from albums al left join songs so ON al.id = so.album_id;

/*d. Join the 'artists' and 'albums' tables. (Find all albums that has letter a in its name)*/
select al.album_title from artists ar 
right join albums al on ar.id = al.artist_id
where album_title like '%a%';

/*e. Sort the albums in Z-A order. (Show only the first 4 records.)*/
select * from albums
order by album_title desc LIMIT 4;

/*f. Join the 'albums' and 'songs' tables. (Sort albums from Z-A)*/
select *
from albums al left join songs so ON al.id = so.album_id
order by al.album_title DESC;
